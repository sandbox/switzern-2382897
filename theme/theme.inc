<?php

function static_progress_meter_preprocess_static_progress_meter(&$variables) {
  drupal_add_css(drupal_get_path('module', 'static_progress_meter') . '/theme/static_progress_meter.css', array('group'=> CSS_DEFAULT));
}